package com.andrjushina.projects.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PageItemsList extends Site implements Pages {

	private String firstOnTheList;
	public static String firstOnTheListName = "Unknown";
	private PageItemsDetails pageItemsDetails;
	
	public PageItemsList() {
		this.firstOnTheList = "//table/tbody/tr[2]/td[@class='msg2']/div/a";
		this.pageItemsDetails = new PageItemsDetails();
	}
	
	public PageItemsList memorizeNameOfFirstAdOnTheList(WebDriver wd) {
		By l = configureALocator(firstOnTheList);
		firstOnTheListName = wd.findElement(l).getText();
		return this;
	}
	
	public PageItemsList memorizeSubstringOfNameOfFirstAdOnTheList(WebDriver wd) {
		By l = configureALocator(firstOnTheList);
		// substring longer than 42 may be longer than actual text
		firstOnTheListName = wd.findElement(l).getText().substring(0, 30);
		return this;
	}
	
	public PageItemsDetails openFirstAdOnTheList(WebDriver wd, int timeout) {
		By l = configureALocator(firstOnTheList);
		pleaseWaitForClickabilityAndClick(wd, timeout, l);
		return pageItemsDetails;
	}
	
}
