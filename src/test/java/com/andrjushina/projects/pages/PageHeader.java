package com.andrjushina.projects.pages;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PageHeader extends Site {
	
	private PageMemo pageMemo;
	private PageSearch pageSearch;
	private String menuOptions;
		
	public PageHeader() {
		this.pageMemo = new PageMemo();
		this.pageSearch = new PageSearch();
		this.menuOptions = "//b[@class='menu_main']/a[@title='"+ getSPLIT_CHAR() +"']";
	}
		
	public Pages getMenuByName(WebDriver wd, int timeout, String menuOption, String browserOfChoice){
		String xpath = configureAnXpath(menuOptions, menuOption);
		By l = configureALocator(xpath);
		if (browserOfChoice.equals("FF")) {
			pleaseWaitForClickabilitySwitchToAndClick(wd, timeout, l);			
		} else if (browserOfChoice.equals("CH")) {
			pleaseWaitForClickabilityAndClick(wd, timeout, l);			
		}
		if (menuOption.equals("Memo")){
			return pageMemo;
		} else if (menuOption.equals("Search")){
			return pageSearch;
		}
		return null;
	}
	
}
