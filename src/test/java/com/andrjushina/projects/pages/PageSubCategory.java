package com.andrjushina.projects.pages;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PageSubCategory extends Site implements Pages {

	private String subCategoryOptions;
	private PageItemsList pageItemsList;

	public PageSubCategory() {
		this.pageItemsList = new PageItemsList();
		this.subCategoryOptions = "//a[text()='"+ getSPLIT_CHAR() +"']";	
	}
	
	public PageItemsList openSubCategoryByName(WebDriver wd, int timeout, String categoryName, String subCategoryName) {		
		String xpath = configureAnXpath(subCategoryOptions, subCategoryName);
		By l = configureALocator(xpath);
		pleaseWaitForClickabilityAndClick(wd, timeout, l);
		return pageItemsList;
	}
	
}
