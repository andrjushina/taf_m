package com.andrjushina.projects.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Site {
	
	// Post-login data only unless said otherwise
	// MISC
		
	protected static String SPLIT_CHAR = "~";
	
	public static String getSPLIT_CHAR() {
		return SPLIT_CHAR;
	}
	
	
	// SMART XPATH AND LOCATOR CONFIG

	public String configureAnXpath(String xpathToConfigure, String flexibleParameter1) {

		String[] originalXpathParts = xpathToConfigure.split(SPLIT_CHAR);
		
		String configuredXpath = originalXpathParts[0].toString() 
				+ flexibleParameter1 
				+ originalXpathParts[1].toString();
		
		return configuredXpath;

		
	}

	
	public String configureAnXpath(String xpathToConfigure, String flexibleParameter1, String flexibleParameter2) {

		String[] originalXpathParts = xpathToConfigure.split(SPLIT_CHAR);
		
		String configuredXpath = originalXpathParts[0].toString() 
				+ flexibleParameter1 
				+ originalXpathParts[1].toString() 
				+ flexibleParameter2 
				+ originalXpathParts[2].toString();
		
		return configuredXpath;

		
	}
	
	
	public String configureAnXpath(String xpathToConfigure, String flexibleParameter1, String flexibleParameter2, String flexibleParameter3) {

		System.out.println(SPLIT_CHAR);
		String[] originalXpathParts = xpathToConfigure.split(SPLIT_CHAR);
		
		String configuredXpath = originalXpathParts[0].toString() 
				+ flexibleParameter1 
				+ originalXpathParts[1].toString() 
				+ flexibleParameter2 
				+ originalXpathParts[2].toString()
				+ flexibleParameter3
				+ originalXpathParts[3].toString();
		
		return configuredXpath;

		
	}
	
	
	public By configureALocator(String configuredXpath) {

		By configuredLocator = new By.ByXPath(configuredXpath);
		
		return configuredLocator;


	}
	
	public WebElement configureWebElement(WebDriver driver, By configuredLocator){
	
		WebElement configuredWebElement = configuredLocator.findElement(driver);
		
		return configuredWebElement;
		
	}
	
	public List<WebElement> configureWebElements(WebDriver driver, By configuredLocator){
		
		List<WebElement> configuredListOfWebElements = configuredLocator.findElements(driver);
		
		return configuredListOfWebElements;
		
	}
	
	//WAITERS
	
	public void pleaseWaitForXMillisec(int howLongMillisec){
		
		try {
			Thread.sleep(howLongMillisec);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	} 
	

	public Boolean pleaseWaitForVisibility(WebDriver driver, int howLongSec, By elementToAppear){
		
		WebDriverWait wait = new WebDriverWait(driver, howLongSec);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(elementToAppear));
		
		Boolean elementVisible = false;
		
			if(element!=null){
				
				elementVisible=true;
			}
		
		return elementVisible;
		
	} 
	
	
	public void pleaseWaitForClickability(WebDriver driver, int timeout, By locator){
		
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.elementToBeClickable(locator));
		
	}

	// borrowed here: https://stackoverflow.com/questions/3401343/scroll-element-into-view-with-selenium
	public WebElement scrollToElementIntoView(WebDriver driver, int timeout, By locator) {
		
		WebElement element = driver.findElement(locator);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		pleaseWaitForClickability(driver, timeout, locator);
		
		return element;
	}
	
	// https://www.guru99.com/scroll-up-down-selenium-webdriver.html
	public void scrollToElementByPoints(WebDriver driver, long points) {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
        driver.manage().window().maximize();
        js.executeScript("window.scrollBy(0,"+ points +")");
		
	}
	
	public void pleaseWaitForClickabilityAndClick(WebDriver driver, int timeout, By locator){
		
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(locator));
		element.click();
		
	}
	
	public void pleaseWaitForClickabilitySwitchToAndClick(WebDriver driver, int timeout, By locator){
		
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		driver.switchTo().frame(element);
		element.click();
		
	}
	
	public void pleaseWaitTheUsualWay(long millis) {
		
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	// STRING COMPARATORS
	
	public Boolean compareStrings(String string1, String string2){
		
		Boolean stringsIdentical = string1.equals(string2);
		
		return stringsIdentical;
		
	}
	
	public Boolean compareStrings(String string1, String string2, String string3){
		
		Boolean stringsIdentical = false;
		
		if(string1.equals(string2) && string2.equals(string3)){
			
			stringsIdentical = true;
			
		}
		
		return stringsIdentical;
		
	}
	
	
	// FUNCTIONAL LOGIC
	
	
	public void openUrl(WebDriver driver, String urlToGet){
		System.out.println("URL to get " + urlToGet);
		
		driver.get(urlToGet);
		pleaseWaitTheUsualWay(1000);
		
	}
	
	

	public String getWhereIAm(WebDriver driver) {
		
		String myLocation = "Nowhere";
		
		myLocation = driver.getCurrentUrl();
		
		return myLocation;
		
	}
	
	public Boolean elementIsDisplayed(WebDriver driver, By locator){
		
		Boolean elementIdDisplayed = false;
		
		elementIdDisplayed = driver.findElement(locator).isDisplayed();
			
		return elementIdDisplayed ;
		
	}

	// Unverified solution, to be used at your own risk
	public void clickElementWithJSE(WebDriver driver, By locator){
		
		WebElement element = driver.findElement(locator);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		
	}
	
	
	public String copyCurrentPage(WebDriver driver){
		
		String pageSource = null;
					
			pageSource = driver.getPageSource();
				
		return pageSource;
		
	}

	
	
	public String copySpecificPageIfCorrect(WebDriver driver, String url){
		
		String pageSource = null;
		
			if (driver.getCurrentUrl()==url){ 
			
				pageSource = driver.getPageSource();
				
			}
		
		System.out.println("Your current url is not the same as the one you are looking for");
		return pageSource;
		
	}

	
}
