package com.andrjushina.projects.pages;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PageItemsDetails extends Site implements Pages {

	private String addToFavorites;
	private String attentionPopUpHeader;
	private String attentionPopUpOkButton;

	public PageItemsDetails() {
		this.addToFavorites = "//a[text()='Add to favorites']";
		this.attentionPopUpOkButton = "//div[@class='alert_body']/a[text()='OK']";
	}
	public PageItemsDetails clickAddToFavorites(WebDriver wd, int timeout) {
		By l = configureALocator(addToFavorites);
		pleaseWaitForClickabilityAndClick(wd, timeout, l);	
		return this;
	}
	public PageItemsDetails closeAttentionPopUp(WebDriver wd, int timeout) {
		By l = configureALocator(attentionPopUpOkButton);
		pleaseWaitForClickabilityAndClick(wd, timeout, l);
		return this;
	}
	
}
