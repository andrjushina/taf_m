package com.andrjushina.projects.pages;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PageLanding extends Site implements Pages{

	private String categoryOptions;
	private PageSubCategory pageSubCategory;
	
	public PageLanding() {
		this.categoryOptions = "//div[./a/img[@title='"+ getSPLIT_CHAR() +"']]/div[@class='main_category']/a[@title='"+ getSPLIT_CHAR() +"']";
		this.pageSubCategory = new PageSubCategory();
	}

	public PageSubCategory openCategoryByName(WebDriver wd, int timeout, String className, String categoryName) {		
		String xpath = configureAnXpath(categoryOptions, className, categoryName);
		By l = configureALocator(xpath);
		pleaseWaitForClickabilityAndClick(wd, timeout, l);
		return pageSubCategory;
	}
	
}
