package com.andrjushina.projects.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PageMemo extends Site implements Pages{
	
	private static Boolean firstOnTheListStatus = false;
	
	//@Override
	public void checkIfFirstOnTheListIsPresent(WebDriver wd, int timeout) {
		String n = PageItemsList.firstOnTheListName;
		String firstOnTheListInMemo = "//div[a[contains(text(), '"+ n +"')] or a/b[contains(text(), '"+ n +"')]]";
		By l = configureALocator(firstOnTheListInMemo);
		pleaseWaitForVisibility(wd, timeout, l);
		if (wd.findElement(l).isDisplayed()) {
			firstOnTheListStatus = true;
		}
	}
		
	public Boolean firstOnTheListIsPresent(WebDriver wd){
		return firstOnTheListStatus;
	}
	
}
