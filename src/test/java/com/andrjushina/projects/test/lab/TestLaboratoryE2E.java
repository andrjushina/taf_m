package com.andrjushina.projects.test.lab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.andrjushina.projects.helpers.DriverHelper;
import com.andrjushina.projects.pages.PageHeader;
import com.andrjushina.projects.pages.PageItemsList;
import com.andrjushina.projects.pages.PageLanding;
import com.andrjushina.projects.pages.PageMemo;
import com.andrjushina.projects.pages.PageSubCategory;
import com.andrjushina.projects.pages.Site;


public class TestLaboratoryE2E extends TestLaboratory{
	
	private DriverHelper driverHelper;
	
	protected Site site;
	protected PageHeader pageHeader;
	protected PageLanding pageLanding;
	protected PageSubCategory pageSubCategory;
	protected PageMemo pageMemo;
	protected PageItemsList pageItemsList;
	protected int timeout;
	

	protected TestLaboratoryE2E() {

		this.driverHelper = new DriverHelper();
		this.site = new Site();
		this.pageHeader = new PageHeader();
		this.pageLanding = new PageLanding();
		this.pageSubCategory = new PageSubCategory();
		this.pageMemo = new PageMemo();
		this.timeout = 2;
	}

	
	// GET DRIVER
	
	protected WebDriver getSpecificDriver(String browserName) {

		return driverHelper.getDriverInstanceByName(browserName);

	}
	
}
