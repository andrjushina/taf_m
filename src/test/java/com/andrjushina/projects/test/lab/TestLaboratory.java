package com.andrjushina.projects.test.lab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestLaboratory {

	//
	// Non-Static Zone
	//
	
	private ClassLoader classLoader = getClass().getClassLoader();
	private File file = new File(classLoader.getResource("taf.properties").getFile());
	
	private Properties readPropsFromFile() {

		Properties props = null;
		
		try {
			props = new Properties();
			props.load(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return props;
	}

	
	// E2E aka Selenium
	
	protected final String baseUrl = readPropsFromFile().getProperty("e2e.base.url");
	protected final String browserOfChoice = readPropsFromFile().getProperty("e2e.browser.of.choice");

	// Rest Common
			
	protected final String mockMode = readPropsFromFile().getProperty("mock.mode");
	protected final String mockType = readPropsFromFile().getProperty("mock.type");
		
	// Rest Common Wiremock Configuration
	
	protected final String mockPreserveHostHeader = readPropsFromFile().getProperty("mock.preserve.host.header");
	protected final String mockStubDataLocation = readPropsFromFile().getProperty("mock.stub.data.location");
	protected final String mockStubDataLocationAbsolute = System.getProperty("user.dir") + "/" + mockStubDataLocation;
		
	
	//
	// Static Zone
	//
	
	// Rest Assures and Wiremock
	
	protected static Properties propsStatic = new Properties();
	protected static InputStream fileStatic = TestLaboratoryRest.class.getClassLoader().getResourceAsStream("taf.properties");

	static protected String restBaseSchemaStatic;
	static protected String restBaseUrlStatic;
	static protected Integer restBasePortStatic;
	static protected String restBasePathStatic;

	static protected String mockSchemaStatic;
	static protected String mockUrlStatic;
	static protected Integer mockPortStatic;
	static protected String mockPathStatic;
	
	private static Properties readPropsFromFileStatic() {

		try {
			propsStatic.load(fileStatic);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return propsStatic;
	}
	
	protected static void setupRestStaticProperties() {
		
		restBaseSchemaStatic = readPropsFromFileStatic().getProperty("rest.base.schema");
		restBaseUrlStatic = readPropsFromFileStatic().getProperty("rest.base.url");
		restBasePortStatic = Integer.parseInt(readPropsFromFileStatic().getProperty("rest.base.port"));
		restBasePathStatic = readPropsFromFileStatic().getProperty("rest.base.path");		
	}
	
	protected static void setupRestStaticPropertiesWithMocks() {
		
		mockSchemaStatic = readPropsFromFileStatic().getProperty("mock.schema");
		mockUrlStatic = readPropsFromFileStatic().getProperty("mock.url");
		mockPortStatic = Integer.parseInt(readPropsFromFileStatic().getProperty("mock.port"));
		mockPathStatic = readPropsFromFileStatic().getProperty("mock.path");
	}
		
}