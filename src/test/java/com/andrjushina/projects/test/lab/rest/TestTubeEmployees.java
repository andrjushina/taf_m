package com.andrjushina.projects.test.lab.rest;

import static com.google.common.truth.Truth.assertThat;

import java.util.List;
import java.util.regex.Pattern;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.andrjushina.projects.test.lab.TestLaboratoryRest;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

import io.restassured.response.Response;


public class TestTubeEmployees extends TestLaboratoryRest {
	
	private Response response;
	private String respbody;
	private String service = "employees";
	
	@BeforeTest(alwaysRun = true)
	protected void configureTest() {
		configureEnvironment(mockMode, mockType, service);
		response = getResponse(service);
		respbody = getResponseBodyAsString(response);
		
	}
	
	@Test(enabled = true, groups = { "smoke", "regression" })
	@Description("Verify that Employees service is available")
	@Severity(SeverityLevel.CRITICAL)
	public void getEmployeesTest001() {
		
		Integer statusCode = response.getStatusCode();
		assertThat(statusCode).isEqualTo(200);
		
	}

	@Test(enabled = true, groups = { "regression" })
	@Description("Verify that Employee service returns 'success' as a status")
	@Severity(SeverityLevel.NORMAL)
	public void getEmployeesTest002() {
		
		String jsonString = "$.status";
		String criterion = JsonPath.using(Configuration.defaultConfiguration()).parse(respbody).read(jsonString,
				String.class);
		Pattern regexp = Pattern.compile("success", Pattern.CASE_INSENSITIVE);
		assertThat(criterion).matches(regexp);
		
	}

	@Test(enabled = true, groups = { "regression" })
	@Description("Verify that Employee service returns correct message for successful outcome")
	@Severity(SeverityLevel.NORMAL)
	public void getEmployeesTest003() {
		
		String jsonString = "$.message";
		String criterion = JsonPath.using(Configuration.defaultConfiguration()).parse(respbody).read(jsonString,
				String.class);
		Pattern regexp = Pattern.compile("Successfully! All records has been fetched.", Pattern.CASE_INSENSITIVE);
		assertThat(criterion).matches(regexp);
		
	}

	@Test(enabled = true, groups = { "regression" })
	@Description("Verify that Employee service returns correct number of employees")
	@Severity(SeverityLevel.CRITICAL)
	public void getEmployeesTest004() {
		
		// This scenario won't work with indefinite paths
		// More: https://github.com/json-path/JsonPath
		Configuration config = Configuration.defaultConfiguration();
		String jsonString = "$.data.length()";
		Integer criterion = JsonPath.using(config).parse(respbody).read(jsonString, Integer.class);
		assertThat(criterion).isEqualTo(24);
		
	}

	@Test(enabled = true, groups = { "regression" })
	@Description("Verify that Employee service returns consistent information about an employee")
	@Severity(SeverityLevel.CRITICAL)
	public void getEmployeesTest005() {

		Configuration config = Configuration.defaultConfiguration();

		DocumentContext ctx = JsonPath.using(config).parse(respbody);

		List<String> employee_name = ctx.read("$.data[?(@.id == '2')].employee_name");
		assertThat(employee_name.size()).isEqualTo(1);
		assertThat(employee_name.get(0)).isEqualTo("Garrett Winters Mock");

		List<Integer> employee_salary = ctx.read("$.data[?(@.id == '2')].employee_salary");
		assertThat(employee_salary.size()).isEqualTo(1);
		assertThat(employee_salary.get(0)).isEqualTo(170750);

		List<Integer> employee_age = ctx.read("$.data[?(@.id == '2')].employee_age");
		assertThat(employee_age.size()).isEqualTo(1);
		assertThat(employee_age.get(0)).isEqualTo(63);

		List<String> profile_image = ctx.read("$.data[?(@.id == '2')].profile_image");
		assertThat(profile_image.size()).isEqualTo(1);
		assertThat(profile_image.get(0)).isEqualTo("");

	}

	@AfterClass(alwaysRun = true)
	public void postConditionUnsetDriver() {
		if(mockMode.equals("mock")) {
			stopMockServer(mockType);
		}
	}
}
