package com.andrjushina.projects.test.lab;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

import java.util.Properties;

import com.andrjushina.projects.helpers.FileReaderHelper;
import com.github.tomakehurst.wiremock.WireMockServer;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class TestLaboratoryRest extends TestLaboratory {
	
	protected WireMockServer wms;
	protected FileReaderHelper reader;
	
	protected Boolean configureEnvironment(String mockMode, String mockType, String service){
		
		Boolean result = false;
				
		if(mockMode.equals("mock")) {
			
			// TODO: Need to support check for invalid combinations of mode and type		
			switch (mockType) {
				case "wiremock":
					setupPropertiesForRestAssuredWithMocks();
					configureAndStartWiremockServer();
					setupWiremockData(service);
					result = true;
					break;
				case "mockserver":
					//TODO
					result = false;
					break;
				default:
					//TODO
					result = false;
					break;
			}
		}
		
		if(mockMode.equals("spy")) {
			//TODO
			result = false;
			
		}
		
		if(mockMode.equals("live")) {
			setupPropertiesForRestAssured();
			result = true;	
		}
					
		return result;
	}
	
	private void configureAndStartWiremockServer() {

		wms = new WireMockServer(options()
				.bindAddress(mockUrlStatic)
				.port(mockPortStatic));
		wms.start();

		configureFor(wms.port());
			
	}
	
	private void setupWiremockData(String service) {

		reader = new FileReaderHelper(mockStubDataLocationAbsolute 
				+ "/" 
				+ service 
				+ ".json");

		stubFor(get(urlEqualTo("/" + service))
				.willReturn(aResponse()
						.withHeader("Content-Type", "application/json")
						.withBody(reader.getFileContentsAsString())));

	}
	
	protected Boolean stopMockServer(String mockType) {

		Boolean result = false;
		
		switch (mockType) {
			case "wiremock":
				wms.stop();
				result = true;
				break;
			case "mockserver":
				//TODO
				result = false;
				break;
			default:
				//TODO
				result = false;
				break;
		}
 
		return result;
	}
	
	private static String buildMockUriForRA() {
		
		String readyUri = null;
		readyUri = mockSchemaStatic + "://" + mockUrlStatic + ":" + mockPortStatic;
		return readyUri;
		
	}

	private static String buildLiveUriForRA() {
		
		String readyUri = null;
		readyUri = restBaseSchemaStatic + "://" + restBaseUrlStatic + ":" + restBasePortStatic + "/" + restBasePathStatic;
		return readyUri;
		
	}

	protected static void setupPropertiesForRestAssured() {
		
		setupRestStaticProperties();		
		RestAssured.baseURI = buildLiveUriForRA();
		
	}
	
	protected static void setupPropertiesForRestAssuredWithMocks() {
		
		setupRestStaticPropertiesWithMocks();
		RestAssured.baseURI = buildMockUriForRA();		
		RestAssured.port = mockPortStatic;

	}
	
	protected Response getResponse(String service) {
				
		Response response = RestAssured.request("GET", "/"+service);
		return response;
		
	}

	protected String getResponseBodyAsString(Response response) {
		
		String respbody = response.body().asString();
		System.out.println("body: " + respbody);		
		return respbody;
		
	}

}