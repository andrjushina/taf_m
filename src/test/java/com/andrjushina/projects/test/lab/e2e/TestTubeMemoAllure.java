package com.andrjushina.projects.test.lab.e2e;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.andrjushina.projects.test.lab.TestLaboratoryE2E;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;

@Epic("The only epic there is")
@Feature("The only feature there is")
public class TestTubeMemoAllure extends TestLaboratoryE2E {
	
	private static WebDriver wd;
	private int timeout = 5;
	
	// always run is required for testng groups to work
	@BeforeTest(alwaysRun = true)
	public void preConditionSetDriver(){	
		wd = getSpecificDriver(browserOfChoice);
		wd.manage().window().maximize();
		if (browserOfChoice.equals("FF")) {
			timeout = 10;
		}
	}
	
	@DataProvider
    public Object[][] getData() {
        return new Object[][]{
        	{"Transport", "Bicycles, scooters", "BMX"}, 
        	{"Real estate", "Houses, cottages", "Jurmala"}};
    }
	
	@Test (enabled = true, dataProvider = "getData", groups = {"smoke", "regression"})
	// next one is junit4 only
	// @DisplayName("Adding an add to memo folder")
	@Description("Lorem ipsum dolor sit amet")
	@Severity(SeverityLevel.NORMAL)
	public void addAdToMemo(String className, String categoryName, String subCategoryName){
		
		wd.get(baseUrl);
		
		pageLanding
			.openCategoryByName(wd, timeout, className, categoryName)
			.openSubCategoryByName(wd, timeout, categoryName, subCategoryName)
			.memorizeSubstringOfNameOfFirstAdOnTheList(wd)
			.openFirstAdOnTheList(wd, timeout)
			.clickAddToFavorites(wd, timeout)
			.closeAttentionPopUp(wd, timeout);
		
		pageHeader
			.getMenuByName(wd, timeout, "Memo", browserOfChoice);
		
		pageMemo
			.checkIfFirstOnTheListIsPresent(wd, timeout);
		
		assertTrue(pageMemo.firstOnTheListIsPresent(wd));

	}
		
	// always run is required for testng groups to work
	@AfterClass(alwaysRun = true)
	public void postConditionUnsetDriver(){
		
		wd.close();
		
	}
	
}