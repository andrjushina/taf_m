package com.andrjushina.projects.helpers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileReaderHelper {
	
	// more on reading files:
	// https://www.geeksforgeeks.org/different-ways-reading-text-file-java/
	
	String target;
	String fileContentsAsString;

	public FileReaderHelper(String target) {
		this.target = target;

		try {
			fileContentsAsString = new String(Files.readAllBytes(Paths.get(target)));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}	

	public String getFileContentsAsString() {
		return fileContentsAsString;
	}
	
	/*
	 * public String readTargetToString() {
	 * 
	 * String fileContentsAsString = ""; try { fileContentsAsString = new
	 * String(Files.readAllBytes(Paths.get(target))); } catch (IOException e) {
	 * e.printStackTrace(); } return fileContentsAsString; }
	 */		
}