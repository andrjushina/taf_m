package com.andrjushina.projects.helpers;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DriverHelper {

	public WebDriver getDriverInstanceByName(String browserName) {

		if (browserName.equals("FF")) {
			
			System.setProperty("webdriver.gecko.driver", getPathToDriver(browserName));

			WebDriver wd = new FirefoxDriver((FirefoxOptions) getOptions(browserName));
			//WebDriver wd = new FirefoxDriver();
			
			System.out.println("Your " + browserName + " driver instance has been generated successfully");
			return wd;

		}
		
		if (browserName.equals("IE")) {
			
			System.setProperty("webdriver.ie.driver", getPathToDriver(browserName));

			//WebDriver wd = new InternetExplorerDriver((InternetExplorerOptions) getOptions(browserName));
			WebDriver wd = new InternetExplorerDriver();

			System.out.println("Your " + browserName + " driver instance has been generated successfully");
			return wd;

		}
		
		if (browserName.equals("CH")) {

			System.setProperty("webdriver.chrome.driver", getPathToDriver(browserName));

			WebDriver wd = new ChromeDriver((ChromeOptions) getOptions(browserName));
			
			//WebDriver wd = new ChromeDriver();
			
			System.out.println("Your " + browserName + " driver instance has been generated successfully");
			
			return wd;
		}
		
		if (!browserName.equals("FF") && !browserName.equals("IE")
				&& !browserName.equals("CH")) {

			System.out.println("Sorry, browser option " + browserName + " is not supported");
		}

		return null;

	}

	private DesiredCapabilities getCapabilities(String browserName) {

		DesiredCapabilities capability = null;

		switch (browserName) {

		case "FF":
			//FirefoxProfile fp = new FirefoxProfile();
			// fp.setPreference(key, value);
			capability = DesiredCapabilities.firefox();
			capability.setCapability("marionette", true);
			//capability.setCapability(FirefoxDriver.PROFILE, fp);
			//System.setProperty("webdriver.gecko.driver", getPathToDriver(browserName));
			break;
		case "IE":
			capability = DesiredCapabilities.internetExplorer();
			//System.setProperty("webdriver.ie.driver", getPathToDriver(browserName));
			break;
		case "CH":
			capability = DesiredCapabilities.chrome();
			//System.setProperty("webdriver.chrome.driver", getPathToDriver(browserName));
			break;

		}

		return capability;
	}

	private MutableCapabilities getOptions(String browserName) {


		if (browserName.equals("FF")) {
			
			String osType = getOsType();

			FirefoxProfile fp = new FirefoxProfile();			
			FirefoxOptions options = new FirefoxOptions();
			//options.setBinary(getPathToDriver(browserName));
			options.setCapability(FirefoxDriver.PROFILE, fp);
			options.setCapability("marionette", true);
			
			if (osType.equals("LNX64")) {
				options.setCapability("platform", Platform.LINUX);
			} else if (osType.equals("WIN64")) {
				options.setCapability("platform", Platform.WINDOWS);
			}
			return options;

		}

		if (browserName.equals("IE")) {

			InternetExplorerOptions options = new InternetExplorerOptions();
			return options;

		}

		if (browserName.equals("CH")) {

			String osType = getOsType();

			ChromeOptions options = new ChromeOptions();
			//options.setBinary(getPathToDriver(browserName));
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-setuid-sandbox");	

			if (osType.equals("LNX64")) {
				options.setCapability("platform", Platform.LINUX);
			} else if (osType.equals("WIN64")) {
				options.setCapability("platform", Platform.WINDOWS);
			}

			return options;
		}

		return null;

	}

	private String getPathToDriver(String browserName) {

		String pathToDriver = null;

		if (getOsType() != null) {

			String osType = getOsType();

			if (osType.equals("WIN64")) {

				switch (browserName) {

				case "FF":
					pathToDriver = "libs/geckodriver.exe";
					break;
				case "IE":
					pathToDriver = "libs/MicrosoftWebDriver.exe";
					break;
				case "CH":
					pathToDriver = "libs/chromedriver.exe";
					System.out.println("Your driver for Chrome is 32bit");
					break;

				default:
					System.out.println("Sorry, something went wrong with a path to a driver");
					break;

				}

			}

			else if (osType.equals("LNX64")) {

				switch (browserName) {

				case "FF":
					pathToDriver = "libs/geckodriver";
					break;
				// case "IE":
				// pathToDriver = null;
				// break;
				case "CH":
					pathToDriver = "libs/chromedriver";
					break;

				default:
					System.out.println("Sorry, something went wrong with a path to a driver");
					break;

				}

			}

		}

		else {

			System.out.println("Sorry, your " + getOsType() + " is not recognised");
		}

		return pathToDriver;

	}

	private String getOsType() {

		// More about system properties:
		// https://docs.oracle.com/javase/tutorial/essential/environment/sysprop.html

		String detectedOsType = System.getProperty("os.name");
		String detectedOsArch = System.getProperty("os.arch");
		String osTypeToReturn = null;

		if (detectedOsType.contains("Windows") && detectedOsArch.contains("amd64")) {

			osTypeToReturn = "WIN64";

		}

		else if (detectedOsType.contains("Linux") && detectedOsArch.contains("amd64")) {

			osTypeToReturn = "LNX64";

		}

		else {

			osTypeToReturn = detectedOsType + " " + detectedOsArch;
			System.out.println("Sorry, your version os OS" + detectedOsType + " " + detectedOsArch
					+ " is not supported at the moment");

		}

		//System.out.println("We've detected that your OS and architecture are " + osTypeToReturn);
		return osTypeToReturn;

	}

}
